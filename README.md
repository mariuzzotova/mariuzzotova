# Мария Зотова
[![Telegram](https://img.shields.io/badge/Telegram-la31367?style=social&logo=telegram)](https://t.me)
[![Gmail](https://img.shields.io/badge/Gmail-la31367?style=social&logo=gmail)](https://gmail.com)
[![LinkedIn](https://img.shields.io/badge/LinkedIn-la31367?style=social&logo=linkedin)](https://gmail.com)




## Тестировщик (ручное + автоматизация)


[Резюме](https://gmail.com)




## Ручное тестирование


![DevTools](https://img.shields.io/badge/DevTools-122929?style=for-the-badges&logo=googlechrome)
![Postman](https://img.shields.io/badge/Postman-122529?style=for-the-badges&logo=postman&logoColor=f7963)
![Swagger](https://img.shields.io/badge/Swagger-122529?style=for-the-badges&logo=postman&logoColor=7ede2b)
![Curl](https://img.shields.io/badge/Curl-122529?style=for-the-badges&logo=curl&logoColor=7ede2b)
![GraphQL](https://img.shields.io/badge/GraphQL_PlayGround-122529?style=for-the-badges&logo=graphql&logoColor=d4088d)
![SoapUI](https://img.shields.io/badge/SoapUI-122529?style=for-the-badges&logo=soapui&logoColor=d4088d)
![GitLab](https://img.shields.io/badge/GitLab_Issues-122529?style=for-the-badges&logo=gitlab)
![GoogleSheets](https://img.shields.io/badge/Google%20Sheets-122529?style=for-the-badges&logo=google-sheets)
![Figma](https://img.shields.io/badge/Figma-122529?style=for-the-badges&logo=figma&logoColor=7d5fa6)
![MySQL](https://img.shields.io/badge/MySQL-122529?style=for-the-badges&logo=mysql)
![PostgreSQL](https://img.shields.io/badge/PostgreSQL-122529?style=for-the-badges&logo=postgresql)
![DBeaver](https://img.shields.io/badge/DBeaver-122529?style=for-the-badges&logo=dbeaver)
![AndroidStudio](https://img.shields.io/badge/AndroidStudio-122529?style=for-the-badges&logo=androidstudio&logoColor=3ad07d)
![Genymotion](https://img.shields.io/badge/Genymotion-122529?style=for-the-badges&logo=genymotion&logoColor=3ad07d)






## Тестовые артефакты


- [Чек-листы ](https://docs.google.com)
- [Тест-кейсы ](https://docs.google.com)
- [Баг-репорты ](https://gitlab.com)
- [Postman коллекции ](https://gitlab.com)
- [SQL-запросы ](https://gitlab.com)


## Автоматизация


![Visial Studio Code](https://img.shields.io/badge/Visial%20Studio%20Code-122529?style=for-the-badges&logo=visual-studio-code&logoColor=248ed5)
![Playwright](https://img.shields.io/badge/Playwright-122529?style=for-the-badges&logo=playwright)
![TypeScript](https://img.shields.io/badge/Typescript-122529?style=for-the-badges&logo=typescript&logoColor=248ed5)
![Git](https://img.shields.io/badge/Git-122529?style=for-the-badges&logo=git&logoColor=dc143c)
![GitLab](https://img.shields.io/badge/GitLab-122529?style=for-the-badges&logo=gitlab)


### Примеры автотестов
- [API ](https://gitlab.com)
- [UI ](https://gitlab.com)


## Сертификаты
- Школа SmartUp -  [удостоверение о переквалификации](https://gitlab.com)
- Инженер по тестированию ПО [сертификат](/assets/сертификат.jpeg)





